#include <stdio.h>

int subtracao(int a, int b) {
	return a - b;
}
int soma(int a, int b){
    return a + b;
}
float divisao(float a, float b){
    return a / b;
}

int multiplicacao(int a, int b){
    return a * b;
}

int main(){
	int a, b, menu;
	float c, d;
	printf ("Insira:\n");
	printf ("1-Soma\n");
	printf ("2-Subtracao\n");
	printf ("3-Divisao\n");
	printf ("4-Multiplicacao\n");
	scanf ("%d", &menu);
	switch(menu){
        case(1):
		    printf ("Insira os 2 números que deseja somar: ");
		    scanf ("%d %d", &a, &b);
		    printf ("Resultado = %d\n", soma(a,b));
            break;
        case(2):
            printf ("Insira os 2 números que deseja subtrair: ");
		    scanf ("%d %d", &a, &b);
		    printf ("Resultado = %d\n", subtracao(a,b));
            break;
        case(3):
            printf ("Insira 2 números que deseja dividir: ");
            scanf("%f %f", &c, &d);
            printf("Resultado = %f\n", divisao(c,d));
            break;
        case(4):
            printf ("Insira os 2 números que deseja multiplicar: ");
		    scanf ("%d %d", &a, &b);
		    printf ("Resultado = %d\n", multiplicacao(a,b));
            break;
        default:
        	printf ("Opcao invalida\n");
            break;
    }
    return 0;
}
